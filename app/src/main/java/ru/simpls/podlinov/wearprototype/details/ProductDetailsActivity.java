package ru.simpls.podlinov.wearprototype.details;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import ru.simpls.podlinov.wearprototype.PagerAdapter;
import ru.simpls.podlinov.wearprototype.R;
import ru.simpls.podlinov.wearprototype.fragments.LastOperationFragment;

/**
 * @author Alexander
 * Детальная информация по продукту (содержит пейджер для слайдина меж фрагментами)
 *
 */

public class ProductDetailsActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_details);
       // doSwitch();
        setupPager();

    }

    void setupPager(){

         ViewPager mPager     = (ViewPager) findViewById(R.id.pager_list);

        final PagerAdapter adapter = new PagerAdapter(getFragmentManager());
        ProductDetailFragment productDetailFragment = new ProductDetailFragment();
        LastOperationFragment lastOperationFragment = new LastOperationFragment();

        adapter.addFragment(productDetailFragment);
        adapter.addFragment(lastOperationFragment);


        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int i, float v, int i2) {
            }

            @Override
            public void onPageSelected(int i) {

            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

        mPager.setAdapter(adapter);
    }




   public void doSwitch(){
      int i = getIntent().getIntExtra("PICKED", 0);
       switch (i){
           case 0:
               //генерим счет
               setupViews();
               break;
           case 1:
               //генерим карту
               break;
           case 2:
               //генерим карту
               break;
           case 3:
               //генерим счет
               break;
           case 4:
               //генерим карту
               break;

           default:

               break;
       }
   }

   void setupViews(){
        ScrollView scrollView1      = (ScrollView)findViewById(R.id.scroll);
        TextView tv_card_acc_num    = (TextView) findViewById(R.id.tv_d_card_acc_num);
        TextView tv_card_number     = (TextView) findViewById(R.id.tv_d_card_number);
        //TextView tv_card_name       = (TextView) findViewById(R.id.tv_d_card_name);
        TextView tv_status          = (TextView) findViewById(R.id.tv_d_status);
        TextView tv_currency        = (TextView) findViewById(R.id.tv_d_currency);
        TextView tv_card_type       = (TextView) findViewById(R.id.tv_card_type);
        TextView tv_card_valid_thru = (TextView) findViewById(R.id.tv_d_card_valid_thru);
        TextView tv_limit           = (TextView) findViewById(R.id.tv_d_limit);

        tv_card_acc_num     .setText(getResources().getString(R.string.strAccNum1));
        tv_card_number      .setText(getResources().getString(R.string.strCardNum1));
        tv_status           .setText("Открыта");
        tv_currency         .setText("RUR");
        tv_card_type        .setText("Visa classic");
        tv_card_valid_thru  .setText("10/15");
        tv_limit            .setText("150 555 RUR");

        scrollView1.setVerticalScrollBarEnabled(false);
    }

    protected void generateCardFields(String name, String number, String validThru, int img){

        LinearLayout ll = (LinearLayout)findViewById(R.id.ll_1);

    }

    protected void generateAccountFields(String name, String number ){

    }
}
