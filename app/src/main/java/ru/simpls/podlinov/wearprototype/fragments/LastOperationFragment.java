package ru.simpls.podlinov.wearprototype.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

import ru.simpls.podlinov.wearprototype.R;

/**
 * Created by Alexander on 26.08.2014.
 * @author Alexander
 * Список последних операций по продукту
 */
public class LastOperationFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.last_operation_list, container, false);
        ListView mList = (ListView)view.findViewById(R.id.lv_operations_details);

        ArrayList<HashMap<String, String>> mData = new ArrayList<HashMap<String, String>>();

        HashMap<String, String> map;

        map = new HashMap<String, String>();
        map.put("Data", "05.6.2014");
        map.put("Name", "Мобильная связь");
        map.put("Amount", "-100 р.");
        mData.add(map);

// Досье на третьего кота
        map = new HashMap<String, String>();
        map.put("Data", "12.07.2014");
        map.put("Name", "Телевидение");
        map.put("Amount", "-490 р.");
        mData.add(map);

        map = new HashMap<String, String>();
        map.put("Data", "12.07.2014");
        map.put("Name", "Платеж");
        map.put("Amount", "-2500 р.");
        mData.add(map);

        map = new HashMap<String, String>();
        map.put("Data", "02.08.2014");
        map.put("Name", "Перевод");
        map.put("Amount", "+2500 р.");
        mData.add(map);

        map = new HashMap<String, String>();
        map.put("Data", "12.09.2014");
        map.put("Name", "WoT");
        map.put("Amount", "-500 р.");
        mData.add(map);

        map = new HashMap<String, String>();
        map.put("Data", "12.10.2014");
        map.put("Name", "Снятие наличных");
        map.put("Amount", "-200 р.");
        mData.add(map);

        SimpleAdapter adapter = new SimpleAdapter(view.getContext(),mData,  R.layout.details_item_list,
                new String[] {"Data", "Name", "Amount"},
                new int[] {R.id.tv_data, R.id.tv_operation_name, R.id.tv_amount});
        mList.setAdapter(adapter);


        return view;
}

}
