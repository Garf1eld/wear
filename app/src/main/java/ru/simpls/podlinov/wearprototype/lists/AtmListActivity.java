package ru.simpls.podlinov.wearprototype.lists;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.activity.ConfirmationActivity;
import android.support.wearable.view.WatchViewStub;
import android.support.wearable.view.WearableListView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ru.simpls.podlinov.wearprototype.R;
import ru.simpls.podlinov.wearprototype.details.AtmDetailsActivity;

/**
 * Created by Alexander on 22.08.2014.
 * @author Alexander
 * Список банкоматов
 */
public class AtmListActivity extends Activity implements WearableListView.ClickListener {

    private WearableListView mListView;
    private MyListAdapter mAdapter;
    private float mDefaultCircleRadius;
    private float mSelectedCircleRadius;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_activity);

        mDefaultCircleRadius = getResources().getDimension(R.dimen.default_settings_circle_radius);
        mSelectedCircleRadius = getResources().getDimension(R.dimen.selected_settings_circle_radius);
        mAdapter = new MyListAdapter();

        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                mListView = (WearableListView) stub.findViewById(R.id.sample_list_view);
                mListView.setAdapter(mAdapter);
                mListView.setClickListener(AtmListActivity.this);
            }
        });
    }

    private static ArrayList<Integer> items;
    static {
        items = new ArrayList<Integer>();
        items.add(R.drawable.ic_atm_red);
        items.add(R.drawable.ic_atm_red);
        items.add(R.drawable.ic_atm_red);
        items.add(R.drawable.ic_atm_red);
        items.add(R.drawable.ic_atm_red);
    }

    private static ArrayList<Integer> atmDistance;
    static {
        atmDistance = new ArrayList<Integer>();
        atmDistance.add(R.string.atmDistance1);
        atmDistance.add(R.string.atmDistance2);
        atmDistance.add(R.string.atmDistance3);
        atmDistance.add(R.string.atmDistance4);
        atmDistance.add(R.string.atmDistance5);
    }

    private static ArrayList<Integer> atmName;
    static {
        atmName = new ArrayList<Integer>();
        atmName.add(R.string.atmName1);
        atmName.add(R.string.atmName1);
        atmName.add(R.string.atmName1);
        atmName.add(R.string.atmName1);
        atmName.add(R.string.atmName1);
    }

    private static ArrayList<Integer> atmAddress;
    static {
        atmAddress = new ArrayList<Integer>();
        atmAddress.add(R.string.atmAddress1);
        atmAddress.add(R.string.atmAddress2);
        atmAddress.add(R.string.atmAddress3);
        atmAddress.add(R.string.atmAddress4);
        atmAddress.add(R.string.atmAddress5);
    }



    @Override
    public void onClick(WearableListView.ViewHolder viewHolder) {

        Intent intent = new Intent(AtmListActivity.this, AtmDetailsActivity.class);

       // items           .get(viewHolder.getPosition());
        atmDistance   .get(viewHolder.getPosition());
        atmName     .get(viewHolder.getPosition());
        atmAddress     .get(viewHolder.getPosition());

        //showSuccessActivity(AtmListActivity.this);

        //intent.putExtra("PICKED", viewHolder.getPosition());
        startActivity(intent);
       // Toast.makeText(this, String.format("You selected item #%s",  viewHolder.getPosition()), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTopEmptyRegionClick() {
        Toast.makeText(this, "You tapped into the empty area above the list", Toast.LENGTH_SHORT).show();
    }

    public class MyListAdapter extends WearableListView.Adapter {

        @Override
        public WearableListView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            return new WearableListView.ViewHolder(new MyItemView(AtmListActivity.this));
        }

        @Override
        public void onBindViewHolder(WearableListView.ViewHolder viewHolder, int position) {
            MyItemView myItemView = (MyItemView) viewHolder.itemView;
         //   final CircledImageView image;
            final TextView tvDistance;
            final TextView tvCompany;
            final TextView tvAddress;

            tvDistance = (TextView) myItemView.findViewById(R.id.tv_atm_distance_val);
            tvCompany = (TextView) myItemView.findViewById(R.id.tv_atm_name);
            tvAddress = (TextView) myItemView.findViewById(R.id.tv_atm_addres);

//            CircledImageView imageView;
//            imageView = (CircledImageView) myItemView.findViewById(R.id.iv_atm_logo);

//            Integer resourceId = items.get(position);
            Integer resourceId_distance = atmDistance.get(position);
            Integer resourceId_name = atmName.get(position);
            Integer resourceId_address = atmAddress.get(position);

            tvDistance.setText(resourceId_distance);
            tvCompany.setText(resourceId_name);
            tvAddress.setText(resourceId_address);

//            imageView.setCircleColor(getResources().getColor(R.color.mts_sea));
//            imageView.setImageResource(resourceId);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }
    }

    private final class MyItemView extends FrameLayout implements WearableListView.Item {

//      final CircledImageView image;
        final TextView tvDistance;
        final TextView tvCompany;
        final TextView tvAddress;
        private float  mScale;

        public MyItemView(Context context) {
            super(context);

            View.inflate(context, R.layout.atm_list_item, this);
//            image               = (CircledImageView) findViewById(R.id.iv_atm_logo);
            tvDistance       = (TextView) findViewById (R.id.tv_atm_distance_val);
            tvCompany    = (TextView) findViewById (R.id.tv_atm_name);
            tvAddress         = (TextView) findViewById (R.id.tv_atm_addres);
        }

        @Override
        public float getProximityMinValue() {
            return mDefaultCircleRadius;
        }

        @Override
        public float getProximityMaxValue() {
            return mSelectedCircleRadius;
        }

        @Override
        public float getCurrentProximityValue() {
            return mScale;
        }

        @Override
        public void setScalingAnimatorValue(float value) {
            mScale = value;
//            image.setCircleRadius(mScale);
//            image.setCircleRadiusPressed(mScale);
        }

        @Override
        public void onScaleUpStart() {
//            image           .setAlpha(1f);
            tvDistance.setAlpha(1f);
            tvCompany.setAlpha(1f);
            tvAddress.setAlpha(1f);
        }

        @Override
        public void onScaleDownStart() {
//            image           .setAlpha(0.5f);
            tvDistance.setAlpha(0.3f);
            tvCompany.setAlpha(0.3f);
            tvAddress.setAlpha(0.3f);
        }
    }

    public static void showSuccessActivity(Activity activity) {
        Intent intent = new Intent(activity, ConfirmationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE, ConfirmationActivity.SUCCESS_ANIMATION);

        activity.startActivity(intent);
    }

}
