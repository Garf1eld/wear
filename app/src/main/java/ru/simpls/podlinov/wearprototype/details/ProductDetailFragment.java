package ru.simpls.podlinov.wearprototype.details;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import ru.simpls.podlinov.wearprototype.R;

/**
 * Created by Alexander on 26.08.2014.
 * @author Alexander
 * Детальная информация по продукту (фрагмент для слайдинга, внутри пейджера )
 */
public class ProductDetailFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.product_details_fragment, container, false);


        ScrollView scrollView1      = (ScrollView)view.findViewById(R.id.scroll);
        TextView tv_card_acc_num    = (TextView) view.findViewById(R.id.tv_d_card_acc_num);
        TextView tv_card_number     = (TextView) view.findViewById(R.id.tv_d_card_number);
        TextView tv_status          = (TextView) view.findViewById(R.id.tv_d_status);
        TextView tv_currency        = (TextView) view.findViewById(R.id.tv_d_currency);
        TextView tv_card_type       = (TextView) view.findViewById(R.id.tv_card_type);
        TextView tv_card_valid_thru = (TextView) view.findViewById(R.id.tv_d_card_valid_thru);
        TextView tv_limit           = (TextView) view.findViewById(R.id.tv_d_limit);

        tv_card_acc_num     .setText(getResources().getString(R.string.strAccNum1));
        tv_card_number      .setText(getResources().getString(R.string.strCardNum1));
        tv_status           .setText("Открыта");
        tv_currency         .setText("RUR");
        tv_card_type        .setText("Visa classic");
        tv_card_valid_thru  .setText("10/15");
        tv_limit            .setText("150 555 RUR");

        scrollView1.setVerticalScrollBarEnabled(false);
        return view;
    }



}
