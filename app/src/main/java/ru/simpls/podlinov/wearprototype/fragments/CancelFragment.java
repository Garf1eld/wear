package ru.simpls.podlinov.wearprototype.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.simpls.podlinov.wearprototype.R;

/**
 * Created by Alexander on 10.09.2014.
 */
public class CancelFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.cancel_layout, container, false);


      return  view;
    }


}
