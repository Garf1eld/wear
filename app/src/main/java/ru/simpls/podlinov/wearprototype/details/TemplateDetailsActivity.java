package ru.simpls.podlinov.wearprototype.details;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableStatusCodes;

import ru.simpls.podlinov.wearprototype.R;
import ru.simpls.podlinov.wearprototype.utils.Utils;

/**
 * Created by Alexander on 01.09.2014.
 *
 * @author Alexander
 *         Детальная информация по шаблону (содержит пейджер, для слайдинга между фрагментами)
 */
public class TemplateDetailsActivity extends Activity {

    String mName;
    String mDestination;
    String mDestinationNumber;
    String mAmount;
    Drawable mSourceIc;
String TAG = "FIRE";

    GoogleApiClient googleApiClient;

    public static final String  START_ACTIVITY_PATH = "/start/MainActivity";

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.templates_details);
        setData();
        setupViews();
       // fireMessage();
        final GoogleApiClient googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();

        googleApiClient.connect();



            }

            private void fireMessage() {
                // Send the RPC
                PendingResult<NodeApi.GetConnectedNodesResult> nodes = Wearable.NodeApi.getConnectedNodes(googleApiClient);
                nodes.setResultCallback(new ResultCallback<NodeApi.GetConnectedNodesResult>() {
                    @Override
                    public void onResult(NodeApi.GetConnectedNodesResult result) {
                        for (int i = 0; i < result.getNodes().size(); i++) {
                            Node node = result.getNodes().get(i);
                            String nName = node.getDisplayName();
                            String nId = node.getId();
                            Log.d(TAG, "Node name and ID: " + nName + " | " + nId);

                            Wearable.MessageApi.addListener(googleApiClient, new MessageApi.MessageListener() {
                                @Override
                                public void onMessageReceived(MessageEvent messageEvent) {
                                    Log.d(TAG, "Message received: " + messageEvent);
                                }
                            });

                            PendingResult<MessageApi.SendMessageResult> messageResult = Wearable.MessageApi.sendMessage(googleApiClient, node.getId(),
                                    START_ACTIVITY_PATH, null);
                            messageResult.setResultCallback(new ResultCallback<MessageApi.SendMessageResult>() {
                                @Override
                                public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                                    Status status = sendMessageResult.getStatus();
                                    Log.d(TAG, "Status: " + status.toString());
                                    if (status.getStatusCode() != WearableStatusCodes.SUCCESS) {


                                    }
                                }
                            });
                        }
                    }

                });
                }



    private void setData() {

        int i = getIntent().getIntExtra("PICKED", 0);
        switch (i) {
            case 0:
                mName = getResources().getString(R.string.strMasterCard);
                mDestination = "ОАО МТС";
                mDestinationNumber = getString(R.string.tmpDesc1);
                mAmount = "500 р.";
                mSourceIc = getResources().getDrawable(R.drawable.ic_maser_card);
                break;
            case 1:
                mName = getResources().getString(R.string.strVisaClassic);
                mDestination = "ЗАО Wargaming";
                mDestinationNumber = getString(R.string.tmpDesc2);
                mAmount = "";
                mSourceIc = getResources().getDrawable(R.drawable.ic_visa);
                break;
            case 2:
                mName = getResources().getString(R.string.strVisaClassicUnembossed);
                mDestination = "ОАО Билайн";
                mDestinationNumber = getString(R.string.tmpDesc3);
                mAmount = "190 р.";
                mSourceIc = getResources().getDrawable(R.drawable.ic_visa);
                break;
            case 3:
                mName = getResources().getString(R.string.strVisaClassic);
                mDestination = "ОАО Билайн";
                mDestinationNumber = getString(R.string.tmpDesc4);
                mAmount = "490 р.";
                mSourceIc = getResources().getDrawable(R.drawable.ic_visa);
                break;
            case 4:
                mName = getResources().getString(R.string.strMasterCard);
                mDestination = "ОАО МТС Банк";
                mDestinationNumber = getString(R.string.tmpDesc5);
                mAmount = "500 р.";
                mSourceIc = getResources().getDrawable(R.drawable.ic_maser_card);
                break;


            default:

                break;
        }
    }

    private void setupViews() {
        ScrollView scrollView1 = (ScrollView) findViewById(R.id.sv_temp_det);

        LinearLayout llDone = (LinearLayout)findViewById(R.id.ll_bt_templ_done);
        TextView tvNumber = (TextView) findViewById(R.id.tv_temp_det_source_num_text);
        TextView tvDestination = (TextView) findViewById(R.id.tv_temp_det_dest_num_text);
        TextView tvDestinationNumber = (TextView) findViewById(R.id.tv_temp_det_dest_num_val);
        TextView tvAmount = (TextView) findViewById(R.id.tv_temp_det_amount_val);
        ImageView ivSourceIc = (ImageView) findViewById(R.id.iv_dest_source_logo);


        llDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showSuccessActivity(TemplateDetailsActivity.this);
               // finish();
            }
        });

        tvNumber.setText(mName);
        tvDestination.setText(mDestination);
        tvDestinationNumber.setText(mDestinationNumber);
        tvAmount.setText(mAmount);
        ivSourceIc.setImageDrawable(mSourceIc);

        scrollView1.setVerticalScrollBarEnabled(false);
    }

    ;

}