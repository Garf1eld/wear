package ru.simpls.podlinov.wearprototype.fragments;

import android.os.Bundle;
import android.support.wearable.view.CardFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.simpls.podlinov.wearprototype.R;

/**
 * Created by Alexander on 10.09.2014.
 */
public class AtmCardFragment extends CardFragment {

    @Override
    public View onCreateContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       // View view = inflater.inflate(R.layout.cancel_layout, container, false);
        ViewGroup mRootView = (ViewGroup) inflater.inflate(R.layout.atm_card_fragment, null);

        mRootView.setPadding(0,0,0,10);
        return  mRootView;
    }


}

