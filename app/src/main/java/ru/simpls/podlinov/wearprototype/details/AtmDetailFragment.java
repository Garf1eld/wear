package ru.simpls.podlinov.wearprototype.details;

import android.os.Bundle;
import android.support.wearable.view.CardFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.simpls.podlinov.wearprototype.R;

/**
 * Created by Alexander on 26.08.2014.
 * @author Alexander
 * Детальная информация о банкомате
 */
public class AtmDetailFragment extends CardFragment {

    @Override
    public View onCreateContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // View view = inflater.inflate(R.layout.cancel_layout, container, false);
        ViewGroup mRootView = (ViewGroup) inflater.inflate(R.layout.atm_details_card_fragment, null);


        return mRootView;
    }
}