package ru.simpls.podlinov.wearprototype.details;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import ru.simpls.podlinov.wearprototype.PagerAdapter;
import ru.simpls.podlinov.wearprototype.R;
import ru.simpls.podlinov.wearprototype.fragments.AtmCardFragment;
import ru.simpls.podlinov.wearprototype.fragments.CancelFragment;

/**
 * Created by Alexander on 04.09.2014.
 */
public class AtmDetailsActivity extends FragmentActivity {

    private ViewPager           mPager;
    private CancelFragment mCancenFragment;
    private AtmDetailFragment mAtmDetailFragment;
    private AtmCardFragment mAtmCardFragment;

    RelativeLayout rl;
    private ImageView mIndicator1;
    private ImageView mIndicator2;
    private ImageView mIndicator3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.atm_details_activity);
          rl = (RelativeLayout)findViewById(R.id.rl_bakground_mountain);

        setupViews();
    }

    private void setupViews() {
        mPager          = (ViewPager) findViewById(R.id.pager);
        mIndicator1     = (ImageView) findViewById(R.id.indicator_0);
        mIndicator2     = (ImageView) findViewById(R.id.indicator_1);
        mIndicator3     = (ImageView) findViewById(R.id.indicator_2);

        final PagerAdapter adapter = new PagerAdapter(getFragmentManager());
        mAtmDetailFragment       = new AtmDetailFragment();
        mCancenFragment            = new CancelFragment();
        mAtmCardFragment        = new AtmCardFragment();
//      mTemplatesPage      = new TemplateFragment();

        adapter.addFragment(mAtmDetailFragment);
        adapter.addFragment(mAtmCardFragment);
        adapter.addFragment(mCancenFragment);
//        adapter.addFragment(mAtmPage);
        setIndicator(0);

        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {
            }

            @Override
            public void onPageSelected(int i) {
                setIndicator(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

        mPager.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void setIndicator(int i) {
        switch (i) {
            case 0:
                mIndicator1.setImageResource(R.drawable.p_act);
                mIndicator2.setImageResource(R.drawable.p_pas);
                mIndicator3.setImageResource(R.drawable.p_pas);
                rl.setAlpha(1f);
                break;
            case 1:
                mIndicator1.setImageResource(R.drawable.p_pas);
                mIndicator2.setImageResource(R.drawable.p_act);
                mIndicator3.setImageResource(R.drawable.p_pas);
                rl.setAlpha(1f);
                break;
            case 2:
                mIndicator1.setImageResource(R.drawable.p_pas);
                mIndicator2.setImageResource(R.drawable.p_pas);
                mIndicator3.setImageResource(R.drawable.p_act);
                rl.setAlpha(0.8f);
                break;
        }
    }

}