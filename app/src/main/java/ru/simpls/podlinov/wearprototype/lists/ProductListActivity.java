package ru.simpls.podlinov.wearprototype.lists;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.view.CircledImageView;
import android.support.wearable.view.WatchViewStub;
import android.support.wearable.view.WearableListView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ru.simpls.podlinov.wearprototype.R;
import ru.simpls.podlinov.wearprototype.details.ProductDetailsActivity;

/**
 * Created by Alexander on 22.08.2014.
 * @author Alexander
 * Список банковских продуктов
 */
public class ProductListActivity extends Activity implements WearableListView.ClickListener {

    private WearableListView mListView;
    private MyListAdapter mAdapter;

    private float mDefaultCircleRadius;
    private float mSelectedCircleRadius;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_activity);

        mDefaultCircleRadius = getResources().getDimension(R.dimen.default_settings_circle_radius);
        mSelectedCircleRadius = getResources().getDimension(R.dimen.selected_settings_circle_radius);
        mAdapter = new MyListAdapter();

        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                mListView = (WearableListView) stub.findViewById(R.id.sample_list_view);
                mListView.setAdapter(mAdapter);
                mListView.setClickListener(ProductListActivity.this);
            }
        });
    }

     private static ArrayList<Integer> items;
    static {
        items = new ArrayList<Integer>();
        items.add(R.drawable.ic_acc);
        items.add(R.drawable.ic_card);
        items.add(R.drawable.ic_card);
        items.add(R.drawable.ic_acc);
        items.add(R.drawable.ic_card);
    }

    private static ArrayList<Integer> productNumber;
    static {
        productNumber = new ArrayList<Integer>();
        productNumber.add(R.string.strAccNum1);
        productNumber.add(R.string.strCardNum1);
        productNumber.add(R.string.strCardNum2);
        productNumber.add(R.string.strAccNum2);
        productNumber.add(R.string.strCardNum3);
    }

    private static ArrayList<Integer> productName;
    static {
        productName = new ArrayList<Integer>();
        productName.add(R.string.strAccount);
        productName.add(R.string.strVisaClassicUnembossed);
        productName.add(R.string.strVisaElectron);
        productName.add(R.string.strAccount);
        productName.add(R.string.strMasterCard);
    }

    private static ArrayList<Integer> productLogo;
    static {
        productLogo = new ArrayList<Integer>();
        productLogo.add(R.drawable.ic_acc);
        productLogo.add(R.drawable.ic_maser_card);
        productLogo.add(R.drawable.ic_visa);
        productLogo.add(R.drawable.ic_acc);
        productLogo.add(R.drawable.ic_maser_card);
    }

    private static ArrayList<String> validThru;
    static {
        validThru = new ArrayList<String>();
        validThru.add("");
        validThru.add("10/16");
        validThru.add("12/14");
        validThru.add("");
        validThru.add("02/18");
    }

    @Override
    public void onClick(WearableListView.ViewHolder viewHolder) {
        //Toast.makeText(this, String.format("You selected item #%s", viewHolder.getPosition()), Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(ProductListActivity.this, ProductDetailsActivity.class);

        items           .get(viewHolder.getPosition());
        productNumber   .get(viewHolder.getPosition());
        productName     .get(viewHolder.getPosition());
        productLogo     .get(viewHolder.getPosition());
        validThru       .get(viewHolder.getPosition());

//        intent.putExtra("PROD_NUM", productNumber);
//        intent.putExtra("PROD_NAME", productName);
//        intent.putExtra("PROD_LOGO", productLogo);
//        intent.putExtra("VALID", validThru);

        intent.putExtra("PICKED", viewHolder.getPosition());
        startActivity(intent);
       // Toast.makeText(this, String.format("You selected item #%s",  viewHolder.getPosition()), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onTopEmptyRegionClick() {
        Toast.makeText(this, "You tapped into the empty area above the list", Toast.LENGTH_SHORT).show();
    }

    public class MyListAdapter extends WearableListView.Adapter {

        @Override
        public WearableListView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            return new WearableListView.ViewHolder(new MyItemView(ProductListActivity.this));
        }

        @Override
        public void onBindViewHolder(WearableListView.ViewHolder viewHolder, int position) {
            MyItemView myItemView = (MyItemView) viewHolder.itemView;
            final CircledImageView image;
            final TextView tvProductType;
            final TextView tvCardLastDigits;
            final TextView tvValidThru;
            final ImageView ivCardLogoMini;

            tvProductType       = (TextView) myItemView.findViewById (R.id.tv_card_name);
            tvCardLastDigits    = (TextView) myItemView.findViewById (R.id.tv_card_last_digits);
            //tvValidThru         = (TextView) myItemView.findViewById (R.id.tv_valid_year);
            ivCardLogoMini      = (ImageView)myItemView.findViewById (R.id.iv_card_logo_mini);

            CircledImageView imageView;
            imageView = (CircledImageView) myItemView.findViewById(R.id.iv_item_logo);


            Integer resourceId          = items.get(position);
            Integer resourceId_number   = productNumber.get(position);
            Integer resourceId_name     = productName.get(position);
            Integer resourceId_logo     = productLogo.get(position);
            String resourceId_valid     = validThru.get(position);

            tvCardLastDigits    .setText(resourceId_number);
            ivCardLogoMini      .setImageResource(resourceId_logo);
            tvProductType       .setText(resourceId_name);
            //tvValidThru         .setText(resourceId_valid);
            imageView.setCircleColor(getResources().getColor(R.color.mts_sea));
            imageView.setImageResource(resourceId);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }
    }

    private final class MyItemView extends FrameLayout implements WearableListView.Item {

        final CircledImageView image;
        final TextView tvProductType;
        final TextView tvCardLastDigits;
        //final TextView tvValidThru;

        private float mScale;
        final ImageView ivCardLogoMini;

        public MyItemView(Context context) {
            super(context);

            View.inflate(context, R.layout.wearablelistview_item, this);
            image               = (CircledImageView) findViewById(R.id.iv_item_logo);
            tvProductType       = (TextView) findViewById (R.id.tv_card_name);
            tvCardLastDigits    = (TextView) findViewById (R.id.tv_card_last_digits);
            //tvValidThru         = (TextView) findViewById (R.id.tv_valid_year);
            ivCardLogoMini      = (ImageView)findViewById (R.id.iv_card_logo_mini);
            ivCardLogoMini.setImageDrawable(getResources().getDrawable(R.drawable.ic_visa));
        }

        @Override
        public float getProximityMinValue() {
            return mDefaultCircleRadius;
        }

        @Override
        public float getProximityMaxValue() {
            return mSelectedCircleRadius;
        }

        @Override
        public float getCurrentProximityValue() {
            return mScale;
        }

        @Override
        public void setScalingAnimatorValue(float value) {
            mScale = value;
            image.setCircleRadius(mScale);
            image.setCircleRadiusPressed(mScale);
        }

        @Override
        public void onScaleUpStart() {
            image.setAlpha(1f);
            tvProductType.setAlpha(1f);
            tvCardLastDigits.setAlpha(1f);
            //tvValidThru.setAlpha(1f);
            ivCardLogoMini.setAlpha(1f);
        }

        @Override
        public void onScaleDownStart() {
            image.setAlpha(0.5f);
            tvProductType.setAlpha(0.5f);
            tvCardLastDigits.setAlpha(0.5f);
            //tvValidThru.setAlpha(0.5f);
            ivCardLogoMini.setAlpha(0.5f);
        }
    }
}
