package ru.simpls.podlinov.wearprototype.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import ru.simpls.podlinov.wearprototype.R;
import ru.simpls.podlinov.wearprototype.lists.TemplateListActivity;

/**
 * Created by Alexander on 21.08.2014.
 * @author Alexander
 * Титульная страница "Мои шаблоны"
 */
public class TemplateFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.templates_fragment, container, false);

        ImageView mTemplatesLogo  = (ImageView) view.findViewById(R.id.iv_templates);
        mTemplatesLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), TemplateListActivity.class));
            }
        });
        return view;
    }
}
