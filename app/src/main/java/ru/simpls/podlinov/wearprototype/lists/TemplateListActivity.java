package ru.simpls.podlinov.wearprototype.lists;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.support.wearable.view.WearableListView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ru.simpls.podlinov.wearprototype.R;
import ru.simpls.podlinov.wearprototype.details.TemplateDetailsActivity;

/**
 * Created by Alexander on 01.09.2014.
 *
 * @author Alexander
 *         Список шаблонов
 */
public class TemplateListActivity extends Activity implements WearableListView.ClickListener {

    private WearableListView mListView;
    private MyListAdapter mAdapter;

    private float mDefaultCircleRadius;
    private float mSelectedCircleRadius;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_activity);

        mDefaultCircleRadius = getResources().getDimension(R.dimen.default_settings_circle_radius);
        mSelectedCircleRadius = getResources().getDimension(R.dimen.selected_settings_circle_radius);
        mAdapter = new MyListAdapter();

        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                mListView = (WearableListView) stub.findViewById(R.id.sample_list_view);
                mListView.setAdapter(mAdapter);
                mListView.setClickListener(TemplateListActivity.this);
            }
        });
    }

    /**
     * Иконки для для шаблонов
     */
    private static ArrayList<Integer> tempIcon;

    static {
        tempIcon = new ArrayList<Integer>();
        tempIcon.add(R.drawable.ic_mob_temp_big);
        tempIcon.add(R.drawable.ic_game_mini);
        tempIcon.add(R.drawable.ic_tv);
        tempIcon.add(R.drawable.ic_internet);
        tempIcon.add(R.drawable.ic_payment_ic_mini);
    }

    private static ArrayList<Integer> tempName;

    static {
        tempName = new ArrayList<Integer>();
        tempName.add(R.string.tmpName1);
        tempName.add(R.string.tmpName2);
        tempName.add(R.string.tmpName3);
        tempName.add(R.string.tmpName4);
        tempName.add(R.string.tmpName5);
    }

    private static ArrayList<Integer> tempProdNum;

    static {
        tempProdNum = new ArrayList<Integer>();
        tempProdNum.add(R.string.tmpDesc1);
        tempProdNum.add(R.string.tmpDesc2);
        tempProdNum.add(R.string.tmpDesc3);
        tempProdNum.add(R.string.tmpDesc4);
        tempProdNum.add(R.string.tmpDesc5);
    }

    private static ArrayList<Integer> tempAmount;

    static {
        tempAmount = new ArrayList<Integer>();
        tempAmount.add(R.string.tmpAmount1);
        tempAmount.add(R.string.tmpAmount2);
        tempAmount.add(R.string.tmpAmount3);
        tempAmount.add(R.string.tmpAmount4);
        tempAmount.add(R.string.tmpAmount5);
    }

    private static ArrayList<Integer> tempColor;

    static {
        tempColor = new ArrayList<Integer>();
        tempColor.add(R.color.mobile);
        tempColor.add(R.color.cayan);
        tempColor.add(R.color.tv);
        tempColor.add(R.color.internet);
        tempColor.add(R.color.green_lite_);
    }

    private static ArrayList<Integer> tempDestLogo;

    static {
        tempDestLogo = new ArrayList<Integer>();
        tempDestLogo.add(R.drawable.ic_payment_ic_mini);
        tempDestLogo.add(R.drawable.ic_payment_ic_mini);
        tempDestLogo.add(R.drawable.ic_payment_ic_mini);
        tempDestLogo.add(R.drawable.ic_payment_ic_mini);
        tempDestLogo.add(R.drawable.ic_payment_ic_mini);
    }


    @Override
    public void onClick(WearableListView.ViewHolder viewHolder) {
        //Toast.makeText(this, String.format("You selected item #%s", viewHolder.getPosition()), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(TemplateListActivity.this, TemplateDetailsActivity.class);
        intent.putExtra("PICKED", viewHolder.getPosition());
        startActivity(intent);
    }

    @Override
    public void onTopEmptyRegionClick() {
        Toast.makeText(this, "You tapped into the empty area above the list", Toast.LENGTH_SHORT).show();
    }

    public class MyListAdapter extends WearableListView.Adapter {

        @Override
        public WearableListView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
            return new WearableListView.ViewHolder(new MyItemView(TemplateListActivity.this));
        }

        @Override
        public void onBindViewHolder(WearableListView.ViewHolder viewHolder, int position) {
            MyItemView myItemView = (MyItemView) viewHolder.itemView;

            final TextView tvTempName;
            final TextView tvTempProductNumber;
            final TextView tvAmount;
            final ImageView ivDestinationLogo;
            final LinearLayout llTempColor;
            final ImageView ivTempLogo;

            llTempColor = (LinearLayout) myItemView.findViewById(R.id.ll_template_color);
            tvTempName = (TextView) myItemView.findViewById(R.id.tv_temp_name);
            tvTempProductNumber = (TextView) myItemView.findViewById(R.id.tv_temp_prod_num);
            tvAmount = (TextView) myItemView.findViewById(R.id.tv_temp_amount);
            //ivDestinationLogo       = (ImageView) myItemView.findViewById(R.id.iv_temp_dest_logo);
            ivTempLogo = (ImageView) myItemView.findViewById(R.id.iv_template_logo);

            Integer tempId_icon = tempIcon.get(position);
            Integer tempId_name = tempName.get(position);
            Integer tempId_prodNumber = tempProdNum.get(position);
            Integer tempId_Amount = tempAmount.get(position);
            Integer tempId_bgColor = tempColor.get(position);
            Integer tempId_destLogo = tempDestLogo.get(position);

            tvTempName.setText(tempId_name);
            tvTempProductNumber.setText(tempId_prodNumber);
            tvAmount.setText(tempId_Amount);
            ivTempLogo.setImageResource(tempId_icon);
            llTempColor.setBackgroundColor(tempId_bgColor);
            //ivDestinationLogo.setImageResource(tempId_destLogo);

            llTempColor.setBackgroundColor(getResources().getColor(tempId_bgColor));
        }

        @Override
        public int getItemCount() {
            return tempName.size();
        }
    }

    private final class MyItemView extends FrameLayout implements WearableListView.Item {

        TextView tvTempName;
        TextView tvTempProductNumber;
        TextView tvAmount;
        ImageView ivDestinationLogo;
        LinearLayout llTempColor;
        ImageView ivTempLogo;
        private float mScale;

        public MyItemView(Context context) {
            super(context);
            View.inflate(context, R.layout.template_list_item, this);

            llTempColor = (LinearLayout) findViewById(R.id.ll_template_color);
            tvTempName = (TextView) findViewById(R.id.tv_temp_name);
            tvTempProductNumber = (TextView) findViewById(R.id.tv_temp_prod_num);
            tvAmount = (TextView) findViewById(R.id.tv_temp_amount);
            //ivDestinationLogo       = (ImageView) findViewById(R.id.iv_temp_dest_logo);
            ivTempLogo = (ImageView) findViewById(R.id.iv_template_logo);
        }

        @Override
        public float getProximityMinValue() {
            return mDefaultCircleRadius;
        }

        @Override
        public float getProximityMaxValue() {
            return mSelectedCircleRadius;
        }

        @Override
        public float getCurrentProximityValue() {
            return mScale;
        }

        @Override
        public void setScalingAnimatorValue(float value) {
            mScale = value;
//            llTempColor.setScaleX(mScale);
//            llTempColor.setScaleY(mScale);
        }

        @Override
        public void onScaleUpStart() {
            llTempColor.setAlpha(1f);
            tvTempName.setAlpha(1f);
            tvTempProductNumber.setAlpha(1f);
            tvAmount.setAlpha(1f);
//            ivDestinationLogo.setAlpha(1f);
            ivTempLogo.setAlpha(1f);
        }

        @Override
        public void onScaleDownStart() {
            llTempColor.setAlpha(0.5f);
            tvTempName.setAlpha(0.5f);
            tvTempProductNumber.setAlpha(0.5f);
            tvAmount.setAlpha(0.5f);
            //        ivDestinationLogo.setAlpha(0.5f);
            ivTempLogo.setAlpha(0.5f);
        }
    }
}
