package ru.simpls.podlinov.wearprototype.utils;

import android.content.Context;
import android.content.Intent;
import android.support.wearable.activity.ConfirmationActivity;

/**
 * Created by Alexander on 09.09.2014.
 */
public class Utils {

    public static void showSuccessActivity(Context context) {
        Intent intent = new Intent(context, ConfirmationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE, ConfirmationActivity.SUCCESS_ANIMATION);

        context.startActivity(intent);
// Animation can be
//                 OPEN_ON_PHONE_ANIMATION
//                 SUCCESS_ANIMATION
//                 FAILURE_ANIMATION
    }

}
