package ru.simpls.podlinov.wearprototype.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import ru.simpls.podlinov.wearprototype.lists.ProductListActivity;
import ru.simpls.podlinov.wearprototype.R;

/**
 * Created by Alexander on 21.08.2014.
 * @author Alexander
 * Титульная страница "Мои финансы"
 */
public class ProductsFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.products_fragment, container, false);

        ImageView mProductsLogo   = (ImageView) view.findViewById(R.id.iv_products);
        mProductsLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ProductListActivity.class));
            }
        });

        return view;
    }
}