package ru.simpls.podlinov.wearprototype;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.widget.ImageView;
import android.widget.TextView;

import ru.simpls.podlinov.wearprototype.fragments.AtmFragment;
import ru.simpls.podlinov.wearprototype.fragments.ProductsFragment;
import ru.simpls.podlinov.wearprototype.fragments.TemplateFragment;

/**
 * @author Alexander
 * Главная активити, содержит пейджер с "титульными" фрагментами
 *
 */
public class MainActivity extends Activity {

    private ViewPager           mPager;
    private ProductsFragment    mProductsPage;
    private AtmFragment         mAtmPage;
    private TemplateFragment    mTemplatesPage;

    private TextView  tvCurPageTitle;
    private ImageView mIndicator1;
    private ImageView mIndicator2;
    private ImageView mIndicator3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pager_layout);

        tvCurPageTitle = (TextView)findViewById(R.id.tv_action_bar_title);
        setupViews();
    }

    private void setupViews() {
        mPager          = (ViewPager) findViewById(R.id.pager);
        mIndicator1     = (ImageView) findViewById(R.id.indicator_0);
        mIndicator2     = (ImageView) findViewById(R.id.indicator_1);
        mIndicator3     = (ImageView) findViewById(R.id.indicator_2);

        final PagerAdapter adapter = new PagerAdapter(getFragmentManager());
        mAtmPage            = new AtmFragment();
        mProductsPage       = new ProductsFragment();
        mTemplatesPage      = new TemplateFragment();

        adapter.addFragment(mProductsPage);
        adapter.addFragment(mTemplatesPage);
        adapter.addFragment(mAtmPage);
        setIndicator(0);

        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {
            }

            @Override
            public void onPageSelected(int i) {
                setIndicator(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

        mPager.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void setIndicator(int i) {
        switch (i) {
            case 0:
                mIndicator1.setImageResource(R.drawable.p_act);
                mIndicator2.setImageResource(R.drawable.p_pas);
                mIndicator3.setImageResource(R.drawable.p_pas);
                tvCurPageTitle.setText(R.string.strFinanceTitle);
                break;
            case 1:
                mIndicator1.setImageResource(R.drawable.p_pas);
                mIndicator2.setImageResource(R.drawable.p_act);
                mIndicator3.setImageResource(R.drawable.p_pas);
                tvCurPageTitle.setText(R.string.strTemplatesTitle);
                break;
            case 2:
                mIndicator1.setImageResource(R.drawable.p_pas);
                mIndicator2.setImageResource(R.drawable.p_pas);
                mIndicator3.setImageResource(R.drawable.p_act);
                tvCurPageTitle.setText(R.string.strAtmsTitle);
                break;
        }
    }

}